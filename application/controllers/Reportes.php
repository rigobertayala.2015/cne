<?php

    class Reportes extends CI_Controller
    {
        function __construct()
        {
          parent::__construct();

          //Cargar modelo de la base de datos de MapaGeneral
          $this->load->model("MapaGeneral");

        }

        public function index(){
          $data['dignidad1']=$this->MapaGeneral->obtenerPresidentes();
          $this->load->view('header');
          $this->load->view('reportes/index',$data);
          $this->load->view('fooder');
        }

        public function nindex(){
          $data['dignidad2']=$this->MapaGeneral->obtenerNacional();
      //FIN
          $this->load->view('header');
          $this->load->view('reportes/nindex',$data);
          $this->load->view('fooder');
        }

        public function pindex(){
          $data['dignidad3']=$this->MapaGeneral->obtenerProvincial();
          $this->load->view('header');
          $this->load->view('reportes/pindex',$data);
          $this->load->view('fooder');
        }

        public function gindex(){
      //FIN
      $data['dignidad1']=$this->MapaGeneral->obtenerPresidentes();
      $data['dignidad2']=$this->MapaGeneral->obtenerNacional();
      $data['dignidad3']=$this->MapaGeneral->obtenerProvincial();
          $this->load->view('header');
          $this->load->view('reportes/gindex',$data);
          $this->load->view('fooder');

        }
        public function guardar(){
      $datosNuevoReportes=array(
        "dignidad_pre"=>$this->input->post('dignidad_pre'),
        "apellido_pre"=>$this->input->post('apellido_pre'),
        "nombre_pre"=>$this->input->post('nombre_pre'),
        "movimiento_pre"=>$this->input->post('movimiento_pre'),
        "edad_pre"=>$this->input->post('edad_pre'),
        "correo_pre"=>$this->input->post('correo_pre'),
        "latitud_pre"=>$this->input->post('latitud_pre'),
        "longitud_pre"=>$this->input->post('longitud_pre')
      );
      }

      }//cierre de la clase
