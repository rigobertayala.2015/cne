<?php

    class Dignidades extends CI_Controller
    {
        function __construct()
        {
          parent::__construct();

          //Cargar modelo
          $this->load->model('IngresarModelo');

        }

        public function nuevo(){
          $this->load->view('header');
          $this->load->view('dignidades/nuevo');
          $this->load->view('fooder');

        }
        public function index(){
          $data['reportes']=$this->IngresarModelo->obtenerTodos();
      //FIN
          $this->load->view('header');
          $this->load->view('reportes/index');
          $this->load->view('fooder');

        }
        public function guardar(){
      $datosNuevoDignidad=array(
        "dignidad_pre"=>$this->input->post('dignidad_pre'),
        "apellido_pre"=>$this->input->post('apellido_pre'),
        "nombre_pre"=>$this->input->post('nombre_pre'),
        "movimiento_pre"=>$this->input->post('movimiento_pre'),
        "edad_pre"=>$this->input->post('edad_pre'),
        "correo_pre"=>$this->input->post('correo_pre'),
        "latitud_pre"=>$this->input->post('latitud_pre'),
        "longitud_pre"=>$this->input->post('longitud_pre')
      );

      if($this->IngresarModelo->insertar($datosNuevoDignidad)){
            redirect('Dignidades/index');
          }else{
            echo "<h1>ERROR AL INSERTAR</h1>";
          }
        }
        //funcion para eliminar instructores

      }//cierre de la clase

    
