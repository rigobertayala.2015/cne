<div class="row" >
  <div class="col-xs-6 col-md-12">
    <h1 class="text-center" style="color: SteelBlue"  >BIENVENIDOS CNE 2023</h1>
  </div>
  </div>
<div id="myCarousel" class="carousel slide " data-ride="carousel" style="background-color: #a9cce3;">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <img src="<?php echo base_url(); ?>/assets/images/cap1.png" width="45%" height="5px" >
    </div>

    <div class="item">
      <img src="<?php echo base_url(); ?>/assets/images/cap2.png" width="50%" height="50px" >
    </div>

    <div class="item">
      <img src="<?php echo base_url(); ?>/assets/images/cap3.png" width="50%" height="50px" >
    </div>
    <div class="item">
      <img src="<?php echo base_url(); ?>/assets/images/cap4.png" width="45%" height="50px" >
    </div>
  </div>


  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">siguiente</span>
  </a>
</div>
<div class="row" >
  <div class="col-xs-6 col-md-12">
    <h1 class="text-center" style="color: SteelBlue"  >Consejo Nacional Electoral</h1>
  </div>
  </div>
<div class="row"  >
  <div class="col-xs-6 col-md-6">
    <table class=" table table-bordered table striped table-hover table responsevi">
    <a href="#" class="thumbnail">
      <img src="<?php echo base_url(); ?>/assets/images/CNE.png" width="100%" height="50px" >
    </a>
    </table>
  </div>
  <div class="col-xs-6 col-md-6" style="background-color: #a9cce3;">
    <table class=" table table-bordered table striped table-hover table responsevi">
      <tr>
        <th>TRAMITES </th>
        <td>Discripción</td>
      </tr>
      <tr>
        <th>Emisión de Certificado de apoliticismo </th>
        <td>El certificado de apoliticismo es un documento que expresa si un ciudadano pertenece o no a un movimiento o partido político el certificado se lo obtiene en coordinación con Dirección Nacional de Organizaciones Polít ... </td>
      </tr>
      <tr>
        <th> Emisión de certificado de Goce de derechos políticos </th>
        <td>políticos 	Este formulario permite solicitar el certificado de goce de derechos políticos, en coordinación con la Dirección Nacional de Organizaciones Políticas y certifica la Secretaría General, éste certificad ... </td>
      </tr>
      <tr>
        <th> Reclamo Administrativo </th>
        <td> El trámite de Reclamo Administrativo sirve para que el ciudadano subsane inconsistencias presentadas Se podrá interponer reclamos administrativos al registro electoral en los siguientes casos:  a) </td>
      </tr>
      <tr>
        <th> Solicitud de desafiliación renuncia </th>
        <td> El formulario de Desafiliación renuncia sirve para que una persona pueda desafiliarse de un movimiento o partido político, siempre y cuando la afiliación fue de forma voluntaria como adherente a un partido o movimiento político,</td>
      </tr>
      <tr>
        <th>Solicitud de Nulidad de Afiliación </th>
        <td> La Nulidad de Afiliación, cabe cuando el ciudadano consta como afiliado a un movimiento o partido político sin haber dado su consentimiento de afiliación. Este formulario permite solicitar se anule de la afiliación del ciudadano, para lo cual, se .</td>
      </tr>
    </table>
  </div>
</div>
<div class="row" >
  <div class="col-xs-6 col-md-12">
    <h1 class="text-center" style="color: SteelBlue"  > AUTORIDADES CNE</h1>
  </div>
  </div>
<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="<?php echo base_url(); ?>assets/images/foro1.png" width="45%" height="50px"  alt="...">
      <div class="caption">
        <h3>Juan Pablo Pozo</h3>
        <p class="text-justify">
- Secretario Relator de la Comisión de Fiscalización y Control Político de la Asamblea Nacional <br>
-Consejero del Consejo Nacional Electoral
</p>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="<?php echo base_url(); ?>assets/images/foto2.png" width="45%" height="50px"  alt="...">
      <div class="caption">
        <h3>  Nubia Mágdala Villacís</h3>
        <p class="text-justify">
        - Instituto Ecuatoriano de Crédito Educativo (IECE)<br>
        - Consejera del Consejo Nacional Electoral <br>
        - Asesor en la Cancilleria
      </p>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="<?php echo base_url(); ?>assets/images/foto3.png" width="45%" height="50px"  alt="...">
      <div class="caption">
        <h3>  Paúl Salazar</h3>
        <p class="text-justify">
          - Asesor en Ministerio del Litoral <br>
          - Coordinador Ministerio de Coordinación de la Política <br>
          - Consejero del Consejo Nacional Electoral
        </p>
      </div>
    </div>
  </div>
</div>
