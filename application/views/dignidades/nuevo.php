<h1 class="text-center" style="color: SteelBlue" > INGRESE NUEVO CANDIDATO/A</h1>
<div class="row" style="background-color: #a9cce3;">
<div class="col-xs-6 col-md-12 text-center" >
  <br>
  <img src="<?php echo base_url(); ?>/assets/images/nuevo.png" width="25%" height="200px" >
  <br>
  <br>
</div>
</div>
<form class="" style="background-color: #a9cce3;"
action="<?php echo site_url(); ?>/dignidades/guardar"
method="post">
    <div class="row">
      <div class="col-md-4">
        <label for="" >DIGNIDAD:</label>
        <select select="selected"
        class="form-control"
        name="dignidad_pre" value="" id="dignidad_pre">
        <option value="Presidente">Presidente</option>
        <option value="Asamblea Nacional">Asamblea Nacional</option>
        <option value="Asamblea Provincial">Asamblea Provincial</option>
        </select>
      </div>
      <div class="col-md-4">
        <label for=""> APELLIDO:</label>
        <br>
        <input type="text"
        placeholder="Ingrese el apellido"
        class="form-control"
        name="apellido_pre" value="" id="apellido_pre">
      </div>
      <div class="col-md-4">
          <label for="">NOMBRE:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre"
          class="form-control"
          name="nombre_pre" value="" id="nombre_pre">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">MOVIMIENTO:</label>
          <br>
          <input type="text"
          placeholder="Ingrese movimiento"
          class="form-control"
          name="movimiento_pre" value="" id="movimiento_pre">
      </div>
      <div class="col-md-4">
        <label for="">EDAD:</label>
        <br>
        <input type="number"
        placeholder="Ingrese la edad"
        class="form-control"
        name="edad_pre" value="" id="edad_pre">
      </div>
      <div class="col-md-4">
          <label for="">CORREO:</label>
          <br>
          <input type="text"
          placeholder="Ingrese su correo"
          class="form-control"
          name="correo_pre" value="" id="correo_pre">
      </div>
    </div>

    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">LATITUD:</label>
          <br>
          <input type="number"
          placeholder="Ingrese latitud"
          class="form-control"
          name="latitud_pre" value="" readonly id="latitud_pre">
      </div>
      <div class="col-md-4">
          <label for="">LONGITUD:</label>
          <br>
          <input type="number"
          placeholder="Ingrese longitud"
          class="form-control"
          name="longitud_pre" value="" readonly  id="longitud_pre">
      </div>
      <div class="col-md-4">
        <label for="" >TIPO:</label>
        <select select="selected"
        class="form-control"
        name="dignidad_pre" value="" id="dignidad_pre">
        <option value="Presidente">Presidente</option>
        <option value="Asamblea Nacional">Asamblea Nacional</option>
        <option value="Asamblea Provincial">Asamblea Provincial</option>
        </select>
      </div>
      <!-- readonly  para poner la ubicacion segun se mueva la ubicacion-->
    </div>
    <br>
    <div class="row">
          <div class="col-md-12 text-center">
              <button type="submit" name="button"
              class="btn btn-primary">
                Guardar
              </button>
              &nbsp;
              <a href="<?php echo site_url(); ?>/dignidades/index"
                class="btn btn-danger">
                Cancelar
              </a>
          </div>
      </div>
<br>
      <!--creacion de mapa -->
<div class="row">
  <div class="col-md-12">
    <div id="mapaUbicacion"
    style="height:300px; width:100%; border:2px solid black;"></div>
  </div>
</div>
<script type="text/javascript">
  function initMap(){
    var centro=new google.maps.LatLng(-0.18933782826197204, -78.48830837572417);
    var mapa1=new google.maps.Map(document.getElementById('mapaUbicacion'),
    {
      center:centro,
		  zoom: 7,
			mapTypeId:google.maps.MapTypeId.ROADMAP
    }
  );
  var marcador =new google.maps.Marker({
    position:centro,
    map:mapa1,
    title: "selecione la direcion",
    icon:"<?php echo base_url('assets/images/icon8.png');?>",
    // draggable para moder el mapa
    draggable: true
  });
  //dar ubicacion donde mueva el marcador
  //esta funcion solo muestra el alert
  google.maps.event.addListener(marcador, 'dragend', function(){
  //alert("se termino el Drag");
  document.getElementById('latitud_pre').value=this.getPosition().lat();
  document.getElementById('longitud_pre').value=this.getPosition().lng();
  });
}//CERRAR LA function initMap() {

</script>
  </form>
