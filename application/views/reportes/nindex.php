
<div class="row">
	<div class="col-md-3"> <center > <img src="<?php echo base_url(); ?>/assets/images/icon.png" width="50%" height="90px" > <center ></div>
	<div class="col-md-6">
		<h1 class="text-center" style="color: SteelBlue"> UBICACIÓN ASAMBLEA NACIONAL</h1>
	</div>
	<div class="col-md-3">   <img src="<?php echo base_url(); ?>/assets/images/lo1.png" width="50%" height="100px" ></div>
</div>
		<div class="row" style="background-color: #a9cce3;"  >
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<div id="mapaLugares" style="height:500px; width:100%; border:2px solid black;"></div>
			</div>
			<div class="col-md-2"></div>
		</div>


		<script type="text/javascript">
			function initMap() {
				var centro=new google.maps.LatLng(-0.932707995429684, -78.6148702956116);
				var mapaLugaresDignidad=new google.maps.Map(document.getElementById('mapaLugares'),
				{
					center:centro,
					zoom: 7,
					mapTypeId:google.maps.MapTypeId.HYBRID
				}
			);
			// cargar el marcador del mapa
      <?php if($dignidad2): ?>
				<?php foreach($dignidad2 as $lugarTemporal): ?>
				var coordenadaTemporal=new google.maps.LatLng(<?php echo $lugarTemporal->latitud_pre; ?>, <?php echo $lugarTemporal->longitud_pre; ?>);

				var marcador=new google.maps.Marker({
					position:coordenadaTemporal,
					title:"<?php echo $lugarTemporal->apellido_pre; ?>, <?php echo $lugarTemporal->nombre_pre; ?> ",
					icon:"<?php echo base_url('assets/images/azul.png');?>",
					map:mapaLugaresDignidad
				});

				<?php endforeach; ?>
				<?php endif; ?>
			}
		</script>
