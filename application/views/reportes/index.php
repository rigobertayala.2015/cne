
<div class="row">
	<div class="col-md-3"> <center > <img src="<?php echo base_url(); ?>/assets/images/icon.png" width="50%" height="90px" > <center ></div>
	<div class="col-md-6">
		<h1 class="text-center" style="color: SteelBlue"> UBICACIÓN PRESIDENTES</h1>
	</div>
	<div class="col-md-3">   <img src="<?php echo base_url(); ?>/assets/images/lo1.png" width="50%" height="100px" ></div>
</div>

		<div class="row" style="background-color: #a9cce3;"  >
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<div id="mapaLugares" style="height:500px; width:100%; border:2px solid black;"></div>
			</div>
			<div class="col-md-2"></div>
		</div>
		<script type="text/javascript">
			function initMap() {
				var centro=new google.maps.LatLng(-0.6018069603234031, -78.64241656120227);
				var mapaLugaresDignidad=new google.maps.Map(document.getElementById('mapaLugares'),
				{
					center:centro,
					zoom: 7,
					mapTypeId:google.maps.MapTypeId.ROADMAP
				}
			);
			// cargar el marcador del mapa
			<?php if($dignidad1): ?>
				<?php foreach($dignidad1 as $lugarTemporal): ?>
				var coordenadaTemporal=new google.maps.LatLng(<?php echo $lugarTemporal->latitud_pre; ?>, <?php echo $lugarTemporal->longitud_pre; ?>);

				var marcador=new google.maps.Marker({
					position:coordenadaTemporal,
					title:"<?php echo $lugarTemporal->apellido_pre; ?>, <?php echo $lugarTemporal->nombre_pre;?>, <?php echo $lugarTemporal->movimiento_pre;?>, <?php echo $lugarTemporal->edad_pre;?>,<?php echo $lugarTemporal->correo_pre;?>",
					icon:"<?php echo base_url('assets/images/rojo.png');?>",
					map:mapaLugaresDignidad
				});

				<?php endforeach; ?>
				<?php endif; ?>
			}
		</script>
